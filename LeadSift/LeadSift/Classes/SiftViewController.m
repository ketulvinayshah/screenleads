/*
 Copyright (c) 2013, salesforce.com, inc. All rights reserved.
 
 Redistribution and use of this software in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this list of conditions
 and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of
 conditions and the following disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of salesforce.com, inc. nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior written
 permission of salesforce.com, inc.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "SiftViewController.h"
#import "STTwitter.h"
#import "SFRestRequest.h"
#import "SFRestAPI.h"

@interface SiftViewController ()

@end

@implementation SiftViewController
@synthesize twitterFeed, textView, currentUser, currentUserId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.prompt = @"Tweets by lead mentioning our product";
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = currentUser;
}


#pragma mark - SFRestAPIDelegate
- (void)request:(SFRestRequest *)request didLoadResponse:(id)jsonResponse {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}
- (void)request:(SFRestRequest*)request didFailLoadWithError:(NSError*)error {
    NSLog(@"request:didFailLoadWithError: %@", error.debugDescription);
    //add your failed error handling here
}
- (void)requestDidCancelLoad:(SFRestRequest *)request {
    NSLog(@"requestDidCancelLoad: %@", request);
    //add your failed error handling here
}
- (void)requestDidTimeout:(SFRestRequest *)request {
    NSLog(@"requestDidTimeout: %@", request);
    //add your failed error handling here
}

- (void)hideKeyboard {
    [textView resignFirstResponder];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self hideKeyboard];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id) initWithUserFeed:(NSString *)twitter_user withId:(NSString *)objectId withProducts:(NSArray *)products{
    self = [super init];
    if (self) {
        currentUser = twitter_user;
        currentUserId = objectId;
        STTwitterAPI *twitter = [STTwitterAPI twitterAPIWithOAuthConsumerName:@"ScreenLeadsApp" consumerKey:@"HkRuCnD5IfLEbB7dmmRYNRCZA" consumerSecret:@"wei5ykqJQXZ6leYZ7Ar8BgXU7vulDANvaRCLUexWViXgDrAIAZ" oauthToken:@"15793980-uhr7GHqbjzvth774F3dLaUfz33SfToQrLvCtlSWlg" oauthTokenSecret:@"DnJqX6A1Fan8KwnoApU5g8WmztUm9Xt14axF0DZD7MZXb"];
        [twitter verifyCredentialsWithSuccessBlock:^(NSString *username) {
            [twitter getUsersSearchQuery:twitter_user page:@"1" count:@"1" includeEntities:0 successBlock:^(NSArray *users) {
                if (users.count > 0) {
                    NSDictionary *firstUser = [users objectAtIndex:0];
                    NSString *screenName = firstUser[@"screen_name"];
                    [twitter getUserTimelineWithScreenName:screenName count:20 successBlock:^(NSArray *statuses) {
                        NSMutableArray *results = [[NSMutableArray alloc] initWithObjects:nil];
                        for (NSDictionary *dictObj in statuses) {
                            NSString *currentTweet = dictObj[@"text"];
                            NSLog(@"%@", currentTweet);
                            for (NSString *currentProduct in products) {
                                NSRange matchIndex = [currentTweet rangeOfString:currentProduct options:NSCaseInsensitiveSearch];
                                if (matchIndex.length > 0) {
                                    [results addObject:dictObj[@"text"]];
                                    break;
                                }
                            }
                        }
                        twitterFeed = [results componentsJoinedByString:@"\n\n"];
                        textView.text = twitterFeed;
                    } errorBlock:^(NSError *error) {
                        NSLog(@"%@", error.debugDescription);
                    }];
                } else {
                    NSLog(@"%@", @"No Users Returned");
                }
            } errorBlock:^(NSError *error) {
                 NSLog(@"%@", error.debugDescription);
            }];
        } errorBlock:^(NSError *error) {
            NSLog(@"%@", error.debugDescription);
        }];
    }
    return self;
}

- (IBAction)update:(id)sender {
    [self updateWithObjectType:@"Lead" objectId:currentUserId];
}

- (void) updateWithObjectType:(NSString *)objectType objectId:(NSString *)objectId{
    NSDictionary *fields = [NSDictionary dictionaryWithObjectsAndKeys:@"Hot", @"Rating", nil];
    SFRestRequest *updateRequest = [[SFRestAPI sharedInstance] requestForUpdateWithObjectType:@"Lead" objectId:objectId fields:fields];
    [[SFRestAPI sharedInstance] send:updateRequest delegate:self];
}


@end
