/*
 Copyright (c) 2011, salesforce.com, inc. All rights reserved.
 
 Redistribution and use of this software in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this list of conditions
 and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of
 conditions and the following disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of salesforce.com, inc. nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior written
 permission of salesforce.com, inc.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#import "LeadViewController.h"

#import "SFRestAPI.h"
#import "SFRestRequest.h"
#import "SiftViewController.h"

@implementation LeadViewController

@synthesize dataRows, productRows;

#pragma mark Misc

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    self.dataRows = nil;
    self.productRows = nil;
}


#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Leads";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //Here we use a query that should work on either Force.com or Database.com
    SFRestRequest *requestForLead = [[SFRestAPI sharedInstance] requestForQuery:@"SELECT Id, Name, Rating FROM Lead"];
    [[SFRestAPI sharedInstance] send:requestForLead delegate:self];
    
    SFRestRequest *requestForProduct = [[SFRestAPI sharedInstance] requestForQuery:@"SELECT Name FROM Product2 WHERE IsActive=true"];
    [[SFRestAPI sharedInstance] send:requestForProduct delegate:self];
}


#pragma mark - SFRestAPIDelegate

- (void)request:(SFRestRequest *)request didLoadResponse:(id)jsonResponse {
    NSArray *records = [jsonResponse objectForKey:@"records"];
    NSDictionary *attributes = [[records objectAtIndex:0] objectForKey:@"attributes"];
    NSString *type = [attributes objectForKey:@"type"];
    
    if ([type isEqual:@"Lead"]) {
        self.dataRows = records;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    } else if([type isEqual:@"Product2"]) {
        NSMutableArray *productBuilder = [[NSMutableArray alloc] initWithObjects: nil];
        for (NSDictionary *obj in records) {
            [productBuilder addObject:[obj objectForKey:@"Name"]];
        }
        self.productRows = productBuilder;
    }
}


- (void)request:(SFRestRequest*)request didFailLoadWithError:(NSError*)error {
    NSLog(@"request:didFailLoadWithError: %@", error);
    //add your failed error handling here
}

- (void)requestDidCancelLoad:(SFRestRequest *)request {
    NSLog(@"requestDidCancelLoad: %@", request);
    //add your failed error handling here
}

- (void)requestDidTimeout:(SFRestRequest *)request {
    NSLog(@"requestDidTimeout: %@", request);
    //add your failed error handling here
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataRows count];
}

- (void)tableView:(UITableView *)itemTableView didSelectRowAtIndexPath:
(NSIndexPath *)indexPath{
    [itemTableView deselectRowAtIndexPath:indexPath animated:NO];
    NSDictionary *obj = [self.dataRows objectAtIndex:indexPath.row];
    SiftViewController *siftViewController = [[SiftViewController alloc] initWithUserFeed:[obj objectForKey:@"Name"] withId:[obj objectForKey:@"Id"] withProducts:productRows];
    [[self navigationController] pushViewController:siftViewController animated:YES];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView_ cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   static NSString *CellIdentifier = @"CellIdentifier";

   // Dequeue or create a cell of the appropriate type.
    UITableViewCell *cell = [tableView_ dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.imageView.image = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
	
	// Configure the cell to show the data.
	NSDictionary *obj = [dataRows objectAtIndex:indexPath.row];
    cell.textLabel.text =  [obj objectForKey:@"Name"];
    
	//this adds the arrow to the right hand side.
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if ([[obj objectForKey:@"Rating"] isEqual:@"Hot"]) {
        UIImage *image = [UIImage imageNamed:@"thumbsup-16.png"];
        cell.imageView.image = image;
    }
    return cell;

}
@end
