public with sharing class ColorFinder {
    
    public ColorFinder(ApexPages.StandardController stdController){
        List<Lead> leads = [SELECT Rating FROM Lead WHERE Id = :stdController.getId()];
        String rating = leads.get(0).Rating;
        if(rating != null){
            if(rating.equals('Hot')){
                circleString = 'green';
            } else if(rating.equals('Warm')){
                circleString = 'yellow';
            }else if(rating.equals('Cold')){
                circleString = 'red';
            }
        }  
    }

    public String circleString {get;set;}
}